#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 2 ] || die "usage: version out_dir"
version=$1; shift
out_dir=$1; shift

checkout()
{
    if [ -d nginx ]; then
        return
    fi

    git clone https://github.com/nginx/nginx
    pushd nginx
    git checkout $version
    git log -n1
    popd
}

OPENSSL_VERSION=3.2.0
PCRE_VERSION=10.42
ZLIB_VERSION=1.3

get_libraries()
{
    pushd nginx
    mkdir -p objs/lib
    pushd objs/lib
    [ -f openssl-${OPENSSL_VERSION}.tar.gz ] || wget https://www.openssl.org/source/openssl-${OPENSSL_VERSION}.tar.gz
    [ -f pcre2-${PCRE_VERSION}.tar.gz ] || wget https://github.com/PCRE2Project/pcre2/releases/download/pcre2-${PCRE_VERSION}/pcre2-${PCRE_VERSION}.tar.gz
    [ -f zlib-${ZLIB_VERSION}.tar.gz ] || wget https://github.com/madler/zlib/releases/download/v${ZLIB_VERSION}/zlib-${ZLIB_VERSION}.tar.gz
    [ -d openssl-${OPENSSL_VERSION} ] || tar xzvf openssl-${OPENSSL_VERSION}.tar.gz
    [ -d pcre2-${PCRE_VERSION} ] || tar xzvf pcre2-${PCRE_VERSION}.tar.gz
    [ -d zlib-${ZLIB_VERSION} ] || tar xzvf zlib-${ZLIB_VERSION}.tar.gz
    popd
    popd
}

build()
{
    pushd nginx
    # http://nginx.org/en/docs/howto_build_on_win32.html
    if [ ! -f configured ]; then
        auto/configure \
            --with-cc=cl \
            --with-debug \
            --prefix= \
            --conf-path=conf/nginx.conf \
            --pid-path=logs/nginx.pid \
            --http-log-path=logs/access.log \
            --error-log-path=logs/error.log \
            --sbin-path=nginx.exe \
            --http-client-body-temp-path=temp/client_body_temp \
            --http-proxy-temp-path=temp/proxy_temp \
            --http-fastcgi-temp-path=temp/fastcgi_temp \
            --http-scgi-temp-path=temp/scgi_temp \
            --http-uwsgi-temp-path=temp/uwsgi_temp \
            --with-cc-opt=-DFD_SETSIZE=1024 \
            --with-pcre=objs/lib/pcre2-${PCRE_VERSION} \
            --with-zlib=objs/lib/zlib-${ZLIB_VERSION} \
            --with-openssl=objs/lib/openssl-${OPENSSL_VERSION} \
            --with-openssl-opt=no-asm \
            --with-http_ssl_module
        touch configured
    fi
    nmake
    # show produced files
    file ./objs/nginx.exe
    ./objs/nginx.exe -V
    popd
}

checkout
get_libraries
build